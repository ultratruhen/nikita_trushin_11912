﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plitki
{
    class Runner
    {
        public int NOD(int a, int b)
        {
            while ((a != 0) && (b != 0))
            {
                if (a > b)
                {
                    a -= b;
                }            
                else
                {
                    b -= a;
                }                   
            }
            return Math.Max(a, b);
        }
        public void Run()
        {
            int n = Convert.ToInt32(Console.ReadLine());
            int m = Convert.ToInt32(Console.ReadLine());
            int nod = NOD(n, m);
                     
           Console.WriteLine(n + m - n);
        }
        public void Run1()
        {
            bool b = true;
            int n = Convert.ToInt32(Console.ReadLine());
            for(int i = 0; i< Math.Sqrt(n); i++)
            {
                if (n % i == 0)
                {
                    b = false;
                    break;
                }
            }
            Console.WriteLine(b);
        }
        public void Run3()
        {
            string s = Console.ReadLine();
            string buf = "";
            for(int i = 0; i < s.Length; i++)
            {
                if (s[i]==' ')
                {
                    
                    Console.Write(buf + " ");
                    buf = "";
                }
                else
                {
                    buf = s[i] + buf;
                }
            }
            Console.Write(buf);
            Console.WriteLine();
        }
        public void Run4()
        {
            int[] arg = Array.ConvertAll(Console.ReadLine().Split(' '), int.Parse);
            int ind = 0;
            for (int i = 0; i < arg.Length; i++)
            {
                if (arg[i] == 0)
                {
                    int buf = arg[ind];
                    arg[ind] = arg[i];
                    arg[i] = buf;
                    ind++;
                }
            }
            for (int i = 0; i < arg.Length; i++)
            {
                if (arg[i] == 1)
                {
                    int buf = arg[ind];
                    arg[ind] = arg[i];
                    arg[i] = buf;
                    ind++;
                }
            }
            foreach(int a in arg)
            {
                Console.Write(a + " ");
            }
        }
        public void Run41()
        {
            int[] arg = Array.ConvertAll(Console.ReadLine().Split(' '), int.Parse);
            int z = 0;
            int o = 0;
            int t = 0;
            for(int i = 0; i<arg.Length;i++)
            {
                if (arg[i] == 0)
                    z++;
                if (arg[i] == 1)
                    o++;
                if (arg[i] == 2)
                    t++;
            }
            for (int i = 0; i < z; i++)
            {
                Console.Write(0 + " ");
            }
            for (int i = 0; i < o; i++)
            {
                Console.Write(1 + " ");
            }
            for (int i = 0; i < t; i++)
            {
                Console.Write(2 + " ");
            }
        }
        int part(int[] arr, int fst, int lst)
        {
            int mrk = fst;
            for (int i = fst; i <= lst; i++)
            {
                if (arr[i] <= arr[lst])
                {
                    int temp = arr[mrk];
                    arr[mrk] = arr[i];
                    arr[i] = temp;
                    mrk += 1;
                }
            }
            return mrk - 1;
        }
        void quicksort(int[] arr, int fst, int lst)
        {
            if (fst >= lst)
            {
                return;
            }
            int spltel = part(arr, fst, lst);
            quicksort(arr, fst, spltel - 1);
            quicksort(arr, spltel + 1, lst);
        }
        public void Run5()
        {
            int[] arg = Array.ConvertAll(Console.ReadLine().Split(' '), int.Parse);
            quicksort(arg, 0, arg.Length-1);
            int dif = 1;
            for(int i = 1; i < arg.Length; i++)
            {
                if (arg[i] != arg[i - 1])
                    dif++;
            }
            Console.WriteLine(dif);
          
        }


        public void Run141()
        {
            int ch = 0, des = 0, f = 0, ed = 0;
            int change = Convert.ToInt32(Console.ReadLine());
            while (change != 0)
            {
                if (change - 25 >= 0)
                {
                    change -= 25;
                    ch++;
                }
                else if (change - 10 >= 0)
                {
                    change -= 10;
                    des++;
                }
                else if (change - 5 >= 0)
                {
                    change -= 5;
                    f++;
                }
                else if (change - 1 >= 0)
                {
                    change -= 1;
                    ed++;
                }
            }
            Console.WriteLine(ch + " " + des + " " + f + " " + ed);
        }
    }
}
