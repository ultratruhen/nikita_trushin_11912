﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreeProject
{
    class Program
    {
        static void Main(string[] args)
        {
            CustomTreeNode<int> bTree = new CustomTreeNode<int>();
            BTreeNode<int> root = new BTreeNode<int>(8, 1, null, null, null);
            BTreeNode<int> l = new BTreeNode<int>();
            BTreeNode<int> r = new BTreeNode<int>();
            l = new BTreeNode<int>(9, 2, new BTreeNode<int>(2, 3, null, null, l), new BTreeNode<int>(5, 4, null, null, l), root);
            r = new BTreeNode<int>(17, 5, new BTreeNode<int>(7, 6, null, null, r), new BTreeNode<int>(35, 7, null, null, r), root);
            root = new BTreeNode<int>(8, 1, l, r, null);
            bTree.root = root;

            bTree.PrintDepth();

             
            bTree.GoWidth();
        }
    }
}
