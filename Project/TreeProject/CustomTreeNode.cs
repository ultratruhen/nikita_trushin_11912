﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreeProject
{
    class CustomTreeNode<T>
    {
        public BTreeNode<T> root;

        /// <summary>
        /// возвращает значение корня дерева
        /// изначально надо было вернуть позицию
        /// </summary>
        public T Root()
        {
            return root != null ? root.Data : default(T);
        }

        /// <summary>
        /// возвращает значение «родителя» для вершины в позиции p
        /// изначально надо было вернуть позицию
        /// </summary>
        public T Parent(int p)
        {
            if (p < 2) throw new ArgumentException("Элемент не имеет родителя");
            List<int> way = new List<int>(); //Путь от позиции p до корня
            while (p > 1)
            {
                way.Insert(0, p);
                p = p / 2;
            }
            var rootCopy = root;
            for (int i = 0; i < way.Count; i++)
            {
                if (way[i] % 2 == 0) //идем в левое поддерево
                {
                    if (rootCopy.Left == null) throw
                             new ArgumentOutOfRangeException($"Элемента на позиции {p} нет");
                    rootCopy = rootCopy.Left;
                }
                else
                {
                    if (rootCopy.Right == null) throw
                             new ArgumentOutOfRangeException($"Элемента на позиции {p} нет");
                    rootCopy = rootCopy.Right;
                }
            }
            return rootCopy != null ? rootCopy.Parent.Data : default(T);
        }

        /// <summary>
        /// возвращает значение «самого левого сына» для вершины в позиции p.
        /// изначально надо было вернуть позицию
        /// </summary>
        public T LeftMostChild(int p)
        {
            BTreeNode<T> b = FindElByPos(p);
            while (b.Left != null)
                b = b.Left;
            return b.Data;
        }

        /// <summary>
        /// возвращает значение «правого брата» для вершины в позиции p.
        /// изначально надо было вернуть позицию
        /// </summary>
        public T RightSibling(int p)
        {
            if (p < 2)
                throw new ArgumentException();
            int w = 0;
            while (p > 1)
            {
                w++;
                p = p / 2;
            }
            if (p != 1)
                throw new ArgumentNullException();
            var r = root;
            for (int i = 0; i < w; i++)
            {
                if (r.Right != null)
                    r = r.Right;
                else if (r.Left != null)
                    r = r.Left;
                else throw new ArgumentNullException();
            }
            return r.Data;
        }

        /// <summary>
        /// возвращает элемент дерева (хранимую информацию) для вершины в позиции p.
        /// </summary>
        public T Element(int p)
        {
            return FindElByPos(p).Data;
        }

        /// <summary>
        /// проверяет, является ли p позицией внутренней вершины (не листа)
        /// </summary>
        public bool IsInternal(int p)
        {
            if (FindElByPos(p).Left != null || FindElByPos(p).Right != null)
                return true;
            return false;
        }

        /// <summary>
        /// проверяет, является ли p позицией листа дерева.
        /// </summary>
        public bool IsExternal(int p)
        {
            if (FindElByPos(p).Left == null && FindElByPos(p).Right == null)
                return true;
            return false;
        }

        /// <summary>
        /// проверяет, является ли p позицией корня.
        /// </summary>
        public bool IsRoot(int p)
        {
            if (root == null) throw new ArgumentException("Дерево пустое");
            return p == 1;
        }

        /// <summary>
        /// проверяет, является ли key ключом корневого узла.
        /// </summary>
        public bool IsRootByKey(int key)
        {
            if (root == null) throw new ArgumentException("Дерево пустое");
            return root.Key == key;
        }
        public BTreeNode<T> FindElByPos(int p)
        {
            List<int> way = new List<int>();
            while (p > 1)
            {
                way.Insert(0, p);
                p = p / 2;
            }
            if (p != 1)
                throw new ArgumentNullException();
            var r = root;
            for (int i = 0; i < way.Count; i++)
            {
                if (way[i] % 2 == 0)
                    r = r.Left;
                else
                    r = r.Right;
            }
            return r;
        }

        public BTreeNode<T> FindEl(int p, BTreeNode<T> r)
        {
            if (p < r.Key)
            {
                if (r.Left != null)
                {
                    return (FindEl(p, r.Left));
                }
                else
                    return null;
            }
            else if (p > r.Key)
            {
                if (r.Right != null)
                {
                    return FindEl(p, r.Right);
                }
                else
                    return null;
            }
            else return r;
        }
        /// <summary>
        /// Поиск элемента в дереве
        /// </summary>
        public bool Find(int key)
        {
            if (FindEl(key, root) == null)
                return false;
            return true;
        }

        /// <summary>
        /// добавление в дерево значения 
        /// </summary>
        public void Insert(int key, T data)
        {
            if (root == null)
            {
                root = new BTreeNode<T>(key, data);
                return;
            }
            var rootCopy = root;
            bool searchPosition = true;
            while (searchPosition)
            {
                if (rootCopy.Key == key)
                {
                    rootCopy.Data = data;
                    return;
                }
                if (rootCopy.Key > key)
                {
                    if (rootCopy.Left == null)
                    {
                        rootCopy.Left = new BTreeNode<T>(key, data, rootCopy);
                        searchPosition = false;
                    }
                    else
                        rootCopy = rootCopy.Left;
                }
                else
                {
                    if (rootCopy.Right == null)
                    {
                        rootCopy.Right = new BTreeNode<T>(key, data, rootCopy);
                        searchPosition = false;
                    }
                    else
                        rootCopy = rootCopy.Right;
                }
            }
        }

        /// <summary>
        /// удаление узла, в котором хранится значение
        /// </summary>
        public void Remove(int key)
        {
            //доделать по желанию
            if (root == null) throw new ArgumentException("Дерево пустое");
            var rootCopy = root;
            var search = true;
            while (search)
            {
                if (rootCopy.Key > key)
                {
                    if (rootCopy.Left == null)
                        throw new ArgumentException($"Элемент с ключом {key} отсутствует");
                    rootCopy = rootCopy.Left;
                }
                else if (rootCopy.Key < key)
                {
                    if (rootCopy.Right == null)
                        throw new ArgumentException($"Элемент с ключом {key} отсутствует");
                    rootCopy = rootCopy.Right;
                }
                else
                {
                    search = false;
                }
            }




            if (rootCopy.Right == null && rootCopy.Left == null)
            {
                var parent = rootCopy.Parent;
                if (parent == null)
                    root = null;
                else
                {
                    if (parent.Right.Key == rootCopy.Key)
                        parent.Right = null;
                    else parent.Left = null;
                }
            }
            if (rootCopy.Right != null && rootCopy.Left == null ||
                rootCopy.Right == null && rootCopy.Left != null)
            {
                var parent = rootCopy.Parent;
                var child = rootCopy.Right != null ? rootCopy.Right : rootCopy.Left;

                if (parent == null)
                {
                    child.Parent = null;
                    root = child;
                }
                else
                {
                    child.Parent = parent;
                    if (parent.Right?.Key == rootCopy.Key)
                        parent.Right = child;
                    else parent.Left = child;
                }
            }
            if (rootCopy.Right != null && rootCopy.Left != null)
            {
                var parent = rootCopy.Parent;
                if (rootCopy.Right.Left == null)
                {
                    rootCopy.Key = rootCopy.Right.Key;
                    rootCopy.Data = rootCopy.Right.Data;
                    if (rootCopy.Right.Right != null)
                        rootCopy.Right.Right.Parent = rootCopy;
                    rootCopy.Right = rootCopy.Right.Right;
                }
                else
                {
                    var mostLeftChild = rootCopy.Right.Left;
                    while (mostLeftChild.Left != null)
                        mostLeftChild = mostLeftChild.Left;
                    rootCopy.Key = mostLeftChild.Key;
                    rootCopy.Data = mostLeftChild.Data;

                    if (mostLeftChild.Right == null)
                    {
                        mostLeftChild.Parent.Left = null;
                    }
                    else
                    {
                        mostLeftChild.Right.Parent = mostLeftChild.Parent;
                        mostLeftChild.Parent.Left = mostLeftChild.Right;
                    }
                }
            }

        }



        /// <summary>
        /// Вывод в глубину прямой
        /// Прямой (pre-order)        
        /// Посетить корень    
        /// Обойти левое поддерево    
        /// Обойти правое поддерево
        /// </summary>
        public void PreOrderPrint()
        {
            PreOrderPrintOneStep(root);
        }

        private void PreOrderPrintOneStep(BTreeNode<T> root)
        {
            if (root == null) return;
            Console.WriteLine(root.Data);
            PreOrderPrintOneStep(root.Left);
            PreOrderPrintOneStep(root.Right);
        }

        /// <summary>
        /// Вывод в глубину Симметричный или поперечный (in-order)
        /// Обойти левое поддерево
        /// Посетить корень
        /// Обойти правое поддерево
        /// </summary>
        public void InOrderPrint()
        {
            if (root == null) throw new ArgumentException();
            InOrderPrint(root);
        }

        public void InOrderPrint(BTreeNode<T> node)
        {
            if (node.Left != null)
            {
                InOrderPrint(node.Left);
            }
            Console.Write(node.Data.ToString() + "  ");
            if (node.Right != null)
            {
                InOrderPrint(node.Right);
            }
        }

        /// <summary>
        /// Вывод в глубину В обратном порядке (post-order)
        /// Обойти левое поддерево
        /// Обойти правое поддерево
        /// Посетить корень
        /// </summary>
        public void PostOrderPrint()
        {
            PostOrderPrintOneStep(root);
        }

        private void PostOrderPrintOneStep(BTreeNode<T> node)
        {
            if (node == null) return;
            PostOrderPrintOneStep(node.Left);
            PostOrderPrintOneStep(node.Right);
            Console.WriteLine(node.Data);
        }

        /// <summary>
        /// Вывод в ширину
        /// </summary>
        public void PrintDepth()
        {
            PrintLevel(new List<BTreeNode<T>>() { root });
        }

        /// <summary>
        /// Вывод одного уровня дерева
        /// </summary>
        /// <param name="list"></param>
        private void PrintLevel(List<BTreeNode<T>> list)
        {
            if (list == null || list.Count == 0)
                return;
            var nextLevel = new List<BTreeNode<T>>();
            foreach (var node in list)
            {
                if (node != null)
                {
                    Console.Write(node.Data + "  ");
                    if (node.Left != null)
                        nextLevel.Add(node.Left);
                    if (node.Right != null)
                        nextLevel.Add(node.Right);
                }
            }
            Console.WriteLine();
            PrintLevel(nextLevel);
        }
        public void GoWidth()
        {
            var r = new Queue<BTreeNode<T>>();
            r.Enqueue(root);
            List<int> min = new List<int>();
            while (r.Count != 0)
            {
                if (r.Peek().Left != null)
                {
                    r.Enqueue(r.Peek().Left);
                }
                if (r.Peek().Right != null)
                {
                    r.Enqueue(r.Peek().Right);
                }
                if (r.Peek().Left == null && r.Peek().Right == null)
                {
                    min.Add(r.Peek().Key);
                }
                r.Dequeue();
            }
            min.Sort();
            Console.WriteLine(min[0]);
        }


        /// <summary> 
        /// Сбалансировать дерево *
        /// </summary>
        public void Balance()
        {

        }

        public void BigRotateLeft()
        {
            root = BigRotateLeft(root);
            Console.WriteLine("После поворота");
            PrintDepth();
        }

        /// <summary>
        /// Большой левый поворот
        /// </summary>
        private BTreeNode<T> BigRotateLeft(BTreeNode<T> oldSubTreeRoot)
        {
            oldSubTreeRoot.Right = RotateRight(oldSubTreeRoot.Right);
            var newSubTreeRoot = RotateLeft(oldSubTreeRoot);
            return newSubTreeRoot;
        }

        /// <summary>
        /// Малый правый поворот
        /// </summary>
        private BTreeNode<T> RotateRight(BTreeNode<T> oldSubTreeRoot)
        {
            var newSubTreeRoot = oldSubTreeRoot.Left;
            newSubTreeRoot.Parent = oldSubTreeRoot.Parent;
            oldSubTreeRoot.Left = newSubTreeRoot.Right;
            oldSubTreeRoot.Left.Parent = oldSubTreeRoot;
            newSubTreeRoot.Right = oldSubTreeRoot;
            oldSubTreeRoot.Parent = newSubTreeRoot;
            return newSubTreeRoot;
        }

        /// <summary>
        /// Малый левый поворот
        /// </summary>
        private BTreeNode<T> RotateLeft(BTreeNode<T> oldSubTreeRoot)
        {
            var newSubTreeRoot = oldSubTreeRoot.Right;
            newSubTreeRoot.Parent = oldSubTreeRoot.Parent;
            oldSubTreeRoot.Right = newSubTreeRoot.Left;
            newSubTreeRoot.Left.Parent = oldSubTreeRoot;
            newSubTreeRoot.Left = oldSubTreeRoot;
            oldSubTreeRoot.Parent = newSubTreeRoot;
            return newSubTreeRoot;
        }

    }
}
