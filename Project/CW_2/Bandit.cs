﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CW_2
{
    public class CrimeEventArgs : EventArgs
    {
        public string callToCC { get; set; }
        public string heyMrPoliceman { get; set; }
        public CrimeEventArgs(string message1, string message2)
        {
            callToCC = message1;
            heyMrPoliceman = message2;
        }
    }
    class Bandit
    {
        public delegate void CrimeHandler(Bandit w, CrimeEventArgs e);
        public event CrimeHandler Delo;
        public string photorobot;
        public string anyweapon;
        public Bandit(string cg,string n)
        {
            photorobot = cg;
            anyweapon = n;
        }
        public void CallTheCops(string s1, string s2)
        {
            Delo?.Invoke(this, new CrimeEventArgs(s1, s2));
        }
    }
    class CallCentre
    {
        public void TimeToMakeJustice(Bandit band, CrimeEventArgs cr)
        {
            Console.WriteLine("Внешность нападавшего - " + band.photorobot);
            Console.WriteLine("Сообщение от потерпевшего - " + cr.callToCC);
        }
        public void Register(Bandit band)
        {
            band.Delo += TimeToMakeJustice;
        }
    }
    class PoliceCar
    {
        public void OperationBegins(Bandit band, CrimeEventArgs cr)
        {
            Console.WriteLine(cr.heyMrPoliceman);
            Console.WriteLine("Вооружение - " + band.anyweapon);
        }
        public void Register(Bandit band)
        {
            band.Delo += OperationBegins;
        }
    }

}
