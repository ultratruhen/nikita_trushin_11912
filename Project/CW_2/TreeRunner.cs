﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CW_2
{
    public class TreeRunner
    {
        BTreeNode root;
        List<int> test = new List<int> { 1, 2, 3, 4, 5, 6, 7 };
        public delegate void delegat();
        public void ChangeColor()
        {
            Console.ForegroundColor = ConsoleColor.Green;
        }


        private void PreOrderPrintOneStep(BTreeNode root)
        {
            if (root == null) return;
            Console.WriteLine(root.Data);
            PreOrderPrintOneStep(root.LeftChild);
            PreOrderPrintOneStep(root.RightChild);
        }
        public void PreOrderPrint(delegat del)
        {
            del();
            PreOrderPrintOneStep(root);
        }
        public void Run()
        {
            delegat d = ChangeColor;
            TreeRunner bTree = new TreeRunner();
            BTreeNode root = new BTreeNode(8, 1, null, null, null);
            BTreeNode l = new BTreeNode();
            BTreeNode r = new BTreeNode();
            l = new BTreeNode(9, 2, new BTreeNode(2, 3, null, null, l), new BTreeNode(5, 4, null, null, l), root);
            r = new BTreeNode(17, 5, new BTreeNode(7, 6, null, null, r), new BTreeNode(35, 7, null, null, r), root);
            root = new BTreeNode(8, 1, l, r, null);
            bTree.root = root;
            bTree.PreOrderPrint(d);
            Console.ResetColor();
        }
        
    }
}
