﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace CW_2
{
    public class ReflectionRunner
    {
        public void Run()
        {
            var asmbl = Assembly.LoadFrom(@"C:\Users\nick_\source\repos\ClassForReflection\ClassForReflection\bin\Debug\ClassForReflection.dll");
            Type type = asmbl.GetType("ClassForReflection.Bandit");
            object obj = System.Activator.CreateInstance(type);
            var fl = BindingFlags.Static | BindingFlags.Public;
            MethodInfo met = type.GetMethod("Tratata", fl);
            met.Invoke(obj, null);
            var fl2 = BindingFlags.NonPublic | BindingFlags.Instance;
            FieldInfo fld = type.GetField("Name", fl2);
            fld.SetValue(obj, "Charles");
            Console.WriteLine(fld.GetValue(obj));
            var fl3 = BindingFlags.Public | BindingFlags.Instance;
            PropertyInfo prop = type.GetProperty("photorobot", fl3);
            prop.SetValue(obj, "какой коренастый американец");
            Console.WriteLine(prop.GetValue(obj));



            Type type2 = asmbl.GetType("ClassForReflection.CallCentre");
            object obj2 = System.Activator.CreateInstance(type2);
            MethodInfo reg = type2.GetMethod("Register");
            MethodInfo ttmj = type2.GetMethod("TimeToMakeJustice");
            EventInfo evnt = type.GetEvent("Delo");
            reg.Invoke(obj2, new object[] { obj });
            MethodInfo doit = type.GetMethod("CallTheCops");
            doit.Invoke(obj, null);
        }
    }
}
