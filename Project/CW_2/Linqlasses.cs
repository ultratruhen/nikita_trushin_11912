﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CW_2

{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
    


    public class Price
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public double Sum { get; set; }
        public bool IsActual { get; set; }
    }



    public class Payment
    {
        public string Name { get; set; }
        public int Count { get; set; }
    }



    public class Sale
    {
        public int Id { get; set; }
        public double percent { get; set; }
    }


    public class SaleType
    {
        public string Name { get; set; }
        public double Before { get; set; }
        public double After { get; set; }
    }


    public class SaleCheck
    {
        public int Aqua { get; set; }
        public int Filter { get; set; }
    }
}
