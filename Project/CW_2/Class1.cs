﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassForReflection
{
    public class CrimeEventArgs : EventArgs
    {
        public string callToCC { get; set; }
        public string heyMrPoliceman { get; set; }
        public CrimeEventArgs(string message1)
        {
            callToCC = message1;
        }
    }
    public class Bandit
    {
        public delegate void CrimeHandler(Bandit w, CrimeEventArgs e);
        public event CrimeHandler Delo;
        public string photorobot { get; set; }
        private string Name;

        public void CallTheCops()
        {
            Delo?.Invoke(this, new CrimeEventArgs("HELP"));
        }
        public static void Tratata()
        {
            Console.WriteLine("haha, minigun do brrrr");
        }
    }
    public class CallCentre
    {
        public void TimeToMakeJustice(Bandit band, CrimeEventArgs cr)
        {
            Console.WriteLine("Внешность нападавшего - " + band.photorobot);
            Console.WriteLine("Сообщение от потерпевшего - " + cr.callToCC);
        }
        public void Register(Bandit band)
        {
            band.Delo += TimeToMakeJustice;
        }
    }
}
