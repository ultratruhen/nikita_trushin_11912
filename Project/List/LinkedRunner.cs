﻿using System;
using System.Collections.Generic;
using System.Text;
using List;

namespace Runner
{
    /// <summary>
    /// Примеры класса связного двунаправленного списка
    /// </summary>
    public class LinkedRunner
    {
        public void Run() 
        {
            var list = new CustomLinkedList<int>();
            int[] arr = { 1, 2, 3, 4, 5, 6, 7 };
            list.Add(7);
            list.Add(7);
            list.Add(7);
            list.Add(7);
            list.Add(8);
            list.Add(10);
            list.Add(7);
            list.Add(7);
            list.Add(7);
            list.Add(7);
            list.Add(3);
            list.AddRange(arr);
            list.Display();
            list.Remove(10);
            list.Display();
            list.Display();
            list.RemoveAll(7);
            list.Display();
            Console.WriteLine(list.Size());
            list.Insert(0, 10);
            list.Display();
            list.Reverse();
            list.Display();
        }
    }
}
