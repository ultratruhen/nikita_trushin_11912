﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using List;

namespace Runner
{
    /// <summary>
    /// Класс для запуска кода 
    /// </summary>
    public class Runner
    {
        /*public void Run()
        {
            try
            {
                var node = LineListUtils.ReadLineList("List1.txt");
               // LineListUtils.WriteLineList(node);

               // LineListUtils.Check(node);



                LineListUtils.WriteLineList(LineListUtils.DeleteNodeWithGivenValue(null,5));
                LineListUtils.WriteLineList(LineListUtils.DeleteNodeWithGivenValue(node, 666));
                LineListUtils.WriteLineList(LineListUtils.DeleteNodeWithGivenValue(node, 3));
                LineListUtils.WriteLineList(LineListUtils.DeleteNodeWithGivenValue(node, 13));
                LineListUtils.WriteLineList(LineListUtils.DeleteNodeWithGivenValue(node, 2));
            }
            catch (FileNotFoundException ex)
            {
                Console.WriteLine(ex.Message + Environment.NewLine + ex.StackTrace);
            }
            catch (IOException)
            {
                Console.WriteLine("Ошибка ввода-вывода");
            }
            catch (Exception ex) {
                Console.WriteLine("Ошибка приложения " + ex.Message);
                throw ex;
            }
        }
        */
        public void Run()
        {
            int[] arr = { 1, 2, 3, 4, 5, 6, 7 };
            int[] arr1 = { 5, 6, 7, 8, 9, 10, 11 };
            CustomList<int> list = new CustomList<int>();
            CustomList<int> list1 = new CustomList<int>();
            list.AddRange(arr);
            list.Display();

            list1.AddRange(arr1);
            list1.Display();

            CustomList<int> res = new CustomList<int>();
            res.Unite(list, list1);
            res.Display();

            CustomList<int> res1 = new CustomList<int>();
            res1.Cross(list, list1);
            res1.Display();

            CustomList<int> res2 = new CustomList<int>();
            res2.Minus(list, list1);
            res2.Display();

            StackOnList l = new StackOnList();
            string s = "(3+7)-(6*4)";
            for (int i = 0; i < s.Length; i++)
            {
                l.Push(s[i]);
            }
            Console.WriteLine(l.CheckOn());

            QueueOnList q = new QueueOnList();
            for (int i = 0; i < arr.Length; i++)
            {
                q.Enqueue(arr[i]);
            }
            q.Out();
        }
    }
}
