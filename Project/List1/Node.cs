﻿using System;

namespace List
{
    /// <summary>
    /// Узел списка 
    /// </summary>
    public class Node2<U>
    {
        /// <summary>
        /// Информационное поле
        /// </summary>
        public U Data { get; set; }

        /// <summary>
        /// Ссылка на следующий узел
        /// </summary>
        public Node2<U> NextNode { get; set; }
    }
}
