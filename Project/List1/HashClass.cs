﻿using System;
using System.Collections.Generic;
using System.Text;

namespace List
{
    public class HashTable
    {
        public HashTable PrevNode { get; set; }
        public HashTable NextNode { get; set; }
        public int Data { get; set; }
        public int Key { get; set; }
    }
}
