﻿using System;
using System.Collections.Generic;
using System.Text;

namespace List
{
    class Node2
    {
        public int Data { get; set; }
        public Node2 NextNode { get; set; }
    }
    public class QueueOnList
    {
        private Node2 head;

        public bool IsEmpty()
        {
            return head == null;
        }

        public void Enqueue(int item)
        {
            if (IsEmpty()) head = new Node2() { Data = item };
            else
            {
                Node2 node = head;
                while (node.NextNode != null) node = node.NextNode;
                node.NextNode = new Node2() { Data = item };
            }
        }

        public int Dequeue()
        {
            if (IsEmpty())
                throw new NullReferenceException();
            int res = head.Data;
            head = head.NextNode;
            return res;
        }

        public int Peek()
        {
            if (IsEmpty())
                throw new NullReferenceException();
            return head.Data;
        }

        public void Out()
        {
            int last;
            int cur = Dequeue();
            if (cur % 2 == 0)
            {
                Console.Write(cur + " ");
            }
            else
            {
                last = cur;
                Enqueue(cur);
            }
            last = cur;
            while (head != null && cur >= last)
            {
                last = cur;
                cur = Dequeue();
                if (cur % 2 == 0)
                {
                    Console.Write(cur + " ");
                }
                else if (cur < last)
                {
                    Console.Write(cur + " ");
                }
                else
                {
                    Enqueue(cur);
                }
                
            }
            while (head != null)
            {
                cur = Dequeue();
                Console.Write(cur + " ");
            }
        }
    }
}
