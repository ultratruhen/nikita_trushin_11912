﻿using System;
using System.Collections.Generic;
using System.Text;

namespace List
{
    /// <summary>
    /// Линейный односвязный список
    /// </summary>
    public class CustomList<T> : ICustomCollection<T> where T : IComparable<T>
    {
        private Node2<T> head;
        ///<inheritdoc/>
        public void Add(T item)
        {
            if (IsEmpty()) head = new Node2<T>() { Data = item };
            else
            {
                Node2<T> node = head;
                while (node.NextNode != null) node = node.NextNode;
                node.NextNode = new Node2<T>() { Data = item };
                ///<inheritdoc/>
            }
        }
        public void AddRange(T[] items)
        {
            if (IsEmpty()) 
            { 
                head = new Node2<T>() { Data = items[0]};
                var node = head;
                for (int i = 1; i < items.Length; i++)
                {
                    node.NextNode = new Node2<T> { Data = items[i] };
                    node = node.NextNode;
                }
            }
            else
            {
                var node = head;
                while (node.NextNode != null) node = node.NextNode;
                for (int i = 0; i < items.Length; i++)
                {
                    node.NextNode = new Node2<T> { Data = items[i] };
                    node = node.NextNode;
                }
            }
        }
        ///<inheritdoc/>
        public void Clear()
        {
            head = null;
        }
        ///<inheritdoc/>
        public bool Contains(T data)
        {
            Node2<T> curNode = head;

            while (curNode != null)
                if (curNode.Data.CompareTo(data) == 0)
                    return true;
                else
                    curNode = curNode.NextNode;

            return false;
        }
        ///<inheritdoc/>
        public int IndexOf(T item)
        {
            Node2<T> curNode = head;
            int k = 0;
            while (curNode != null)
                if (curNode.Data.CompareTo(item) == 0)
                    return k;
                else
                {
                    k++;
                    curNode = curNode.NextNode;
                }
            return -1;
        }
        ///<inheritdoc/>
        public void Insert(int index, T item)
        {
            Node2<T> Ins = new Node2<T>();
            Ins.Data = item;
            if (index < 0 || index > (Size()-1))
                throw new IndexOutOfRangeException();
            if (index == 0)
            {
                Ins.NextNode = head;
                head = Ins;
            }
            else
            {
                var node = head;
                for (var i = 0; i < index - 1; i++)
                {
                    node = node.NextNode;
                }
                Ins.NextNode = node.NextNode;
                node.NextNode = Ins;               
            }
        }
        ///<inheritdoc/>
        public bool IsEmpty()
        {
            return head == null;
        }
        ///<inheritdoc/>
        public void Remove(T item)
        {
            //проверка списка на пустоту
            if (IsEmpty())
            {
                return;
            }

            //Значение - головной элемент
            if (head.Data.CompareTo(item) == 0)
            {
                head = head.NextNode;
                return;
            }

            //идем до конца списка, рассматриваем 2 случая:

            //значение отсутствует, возвращаем сообщение об отсутствии эл-та
            //удаляем соответствующий элемент
            Node2<T> prevNode = null;
            Node2<T> node = head;
            bool found = false;

            while (node != null && !found)
            {
                if (node.Data.CompareTo(item) == 0)
                    found = true;
                else
                {
                    prevNode = node;
                    node = node.NextNode;
                }
            }
            if (found) prevNode.NextNode = node.NextNode;

            return;
        }
        ///<inheritdoc/>
        public void RemoveAll(T item)
        {
            if (IsEmpty())
            {
                return;
            }
            while (head.Data.CompareTo(item) == 0)
            {
                head = head.NextNode;
            }
            var node = head;
            while (node.NextNode != null)
            {
                if (node.NextNode.Data.CompareTo(item) == 0 && node.NextNode.NextNode == null)
                {
                    node.NextNode = null; ;
                }
                else if (node.NextNode.Data.CompareTo(item) == 0 && node.NextNode.NextNode != null)
                {
                    node.NextNode = node.NextNode.NextNode;
                }
                else
                {
                    node = node.NextNode;
                }
            }
           
        }
        ///<inheritdoc/>
        public void RemoveAt(int index)
        {
            Node2<T> curNode = head;
            if (index < 0 || index > (Size() - 1))
                throw new IndexOutOfRangeException();
            if (index == 0)
            {
                head = head.NextNode;
            }
            else
            {
                for (int i = 0; i < index - 1; i++)
                {
                    curNode = curNode.NextNode;
                }
                curNode.NextNode = curNode.NextNode.NextNode;
            }

        }
        ///<inheritdoc/>
        public void Reverse()
        {
            Node2<T> curNode = head;
            Node2<T> nextNode = null;
            Node2<T> prevNode = null;
            while (curNode != null)
            {
                nextNode = curNode.NextNode;
                curNode.NextNode = prevNode;
                prevNode = curNode;
                curNode = nextNode;
            }
            head = prevNode;
        }
        ///<inheritdoc/>
        public int Size()
        {
            if (head == null)
                return 0;
            int count = 1;
            Node2<T> curNode = head;
            while (curNode.NextNode != null)
            {
                count++;
                curNode = curNode.NextNode;
            }
            return count;
        }
        ///<inheritdoc/>
        public void Display()
        {
            if (!IsEmpty())
            {
                Node2<T> node = head;
                while (node != null)
                {
                    Console.Write(node.Data + " ");
                    node = node.NextNode;
                }

                Console.WriteLine();
            }
            else Console.WriteLine("list empty");
        }

        public CustomList<T> Unite(CustomList<T> n1, CustomList<T> n2)
        {
            var curNode1 = n1.head;
            var curNode2 = n2.head;
            if (n1.head.Data.CompareTo(n2.head.Data) == -1)
            {
                curNode1 = curNode1.NextNode;
                head = new Node2<T>();
                head.Data = n1.head.Data;
            }
            else if (n1.head.Data.CompareTo(n2.head.Data) == 0)
            {
                curNode1 = curNode1.NextNode;
                curNode2 = curNode2.NextNode;
                head = new Node2<T>();
                head.Data = n1.head.Data;
            }
            else
            {
                curNode2 = curNode2.NextNode;
                head = new Node2<T>();
                head.Data = n2.head.Data;
            }
            CustomList<T> curNode3 = new CustomList<T>();
            curNode3.head = head;
            while (curNode1 != null && curNode2 != null)
            {
                if (curNode1.Data.CompareTo(curNode2.Data) == -1)
                {
                    curNode3.head.NextNode = new Node2<T>();
                    curNode3.head.NextNode.Data = curNode1.Data;
                    curNode3.head = curNode3.head.NextNode;
                    curNode1 = curNode1.NextNode;
                }
                else if(curNode1.Data.CompareTo(curNode2.Data) == 0)
                {
                    curNode3.head.NextNode = new Node2<T>();
                    curNode3.head.NextNode.Data = curNode1.Data;
                    curNode3.head = curNode3.head.NextNode;
                    curNode1 = curNode1.NextNode;
                    curNode2 = curNode2.NextNode;
                }
                else
                {
                    curNode3.head.NextNode = new Node2<T>();
                    curNode3.head.NextNode.Data = curNode2.Data;
                    curNode3.head = curNode3.head.NextNode;
                    curNode2 = curNode2.NextNode;
                }
            }
            if (curNode1 != null)
            {
                while (curNode1 != null)
                {
                    curNode3.head.NextNode = new Node2<T>();
                    curNode3.head.NextNode.Data = curNode1.Data;
                    curNode3.head = curNode3.head.NextNode;
                    curNode1 = curNode1.NextNode;
                }
            }
            else if (curNode2 != null)
            {
                while (curNode2 != null)
                {
                    curNode3.head.NextNode = new Node2<T>();
                    curNode3.head.NextNode.Data = curNode2.Data;
                    curNode3.head = curNode3.head.NextNode;
                    curNode2 = curNode2.NextNode;
                }
            }
            return curNode3;
        }

        public CustomList<T> Cross(CustomList<T> n1, CustomList<T> n2)
        {
            var curNode1 = n1.head;
            var curNode2 = n2.head;
            while ((curNode1 != null && curNode2 != null))
            {
                if(curNode1.Data.CompareTo(curNode2.Data) == -1)
                {
                    curNode1 = curNode1.NextNode;
                }
                else if(curNode1.Data.CompareTo(curNode2.Data) == 1)
                {
                    curNode2 = curNode2.NextNode;
                }
                else
                {
                    break;
                }
            }
            if (curNode1 == null || curNode2 == null)
            {
                Console.WriteLine("пересечения не найдено");
                return null;
            }
            head = new Node2<T>();
            head.Data = curNode1.Data;
            CustomList<T> curNode3 = new CustomList<T>();
            curNode3.head = head;
            curNode1 = curNode1.NextNode;
            curNode2 = curNode2.NextNode;
            while (curNode1 != null && curNode2 != null)
            {
                if (curNode1.Data.CompareTo(curNode2.Data) == -1)
                {
                    curNode1 = curNode1.NextNode;
                }
                else if (curNode1.Data.CompareTo(curNode2.Data) == 1)
                {
                    curNode2 = curNode2.NextNode;
                }
                else
                {
                    curNode3.head.NextNode = new Node2<T>();
                    curNode3.head.NextNode.Data = curNode1.Data;
                    curNode3.head = curNode3.head.NextNode;
                    curNode1 = curNode1.NextNode;
                    curNode2 = curNode2.NextNode;
                }
            }
            return curNode3;
        }

        public CustomList<T> Minus(CustomList<T> n1, CustomList<T> n2)
        {
            var curNode1 = n1.head;
            var curNode2 = n2.head;
            while ((curNode1 != null && curNode2 != null))
            {
                if (curNode1.Data.CompareTo(curNode2.Data) != 0)
                {
                    break;
                }
                else 
                {
                    curNode1 = curNode1.NextNode;
                    curNode2 = curNode2.NextNode;
                }               
            }
            if (curNode1 == null || curNode2 == null)
            {
                Console.WriteLine("множества идентичны");
                return null;
            }
            head = new Node2<T>();
            head.Data = curNode1.Data;
            CustomList<T> curNode3 = new CustomList<T>();
            curNode3.head = head;
            curNode1 = curNode1.NextNode;
            while (curNode1 != null && curNode2 != null)
            {
                if (curNode1.Data.CompareTo(curNode2.Data) == -1)
                {
                    curNode3.head.NextNode = new Node2<T>();
                    curNode3.head.NextNode.Data = curNode1.Data;
                    curNode3.head = curNode3.head.NextNode;
                    curNode1 = curNode1.NextNode;
                }
                else if (curNode1.Data.CompareTo(curNode2.Data) == 1)
                {
                    curNode2 = curNode2.NextNode;
                }
                else
                {
                    curNode1 = curNode1.NextNode;
                    curNode2 = curNode2.NextNode;
                }
            }
            if (curNode1 != null)
            {
                while (curNode1 != null)
                {
                    curNode3.head.NextNode = new Node2<T>();
                    curNode3.head.NextNode.Data = curNode1.Data;
                    curNode3.head = curNode3.head.NextNode;
                    curNode1 = curNode1.NextNode;
                }
            }
            return curNode3;
        }
    }
    
}
