﻿using System;
using System.Collections.Generic;
using System.Text;

namespace List
{
    class Node1
    {
        public char Data { get; set; }
        public Node1 NextNode { get; set; }
    }
    public class StackOnList
    {
        private Node1 head;

        public bool IsEmpty()
        {
            return head == null;
        }

        public void Push(char item)
        {
            if (IsEmpty()) head = new Node1() { Data = item };
            else
            {
                Node1 Ins = new Node1();
                Ins.Data = item;
                Ins.NextNode = head;
                head = Ins;
            }
        }
        public char Pull()
        {
            if (IsEmpty())
                throw new NullReferenceException();
            char res = head.Data;
            head = head.NextNode;
            return res;
        }
        public char Peek()
        {
            if (IsEmpty())
                throw new NullReferenceException();
            return head.Data;
        }
        
        public bool CheckOn()
        {
            int op = 0;
            int cl = 0;
            char prev = Pull();
            if (prev == ')')
                cl++;
            else if (prev == '(')
                return false;
            while (head != null)
            {
                char cur = Pull();
                if(cur == '(')
                {
                    op++;
                }
                else if (cur == ')')
                {
                    cl++;
                }
                if (op > cl)
                    return false;
                if (prev == ')' && (cur == '+' || cur == '-' || cur == '*' || cur == '/'))
                    return false;
                if ((prev == '+' || prev == '-' || prev == '*' || prev == '/') && cur == '(')
                    return false;
                prev = cur;
            }
            return true;
        }

    }
}
