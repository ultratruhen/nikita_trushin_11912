﻿using System;
using System.Collections.Generic;
using System.Text;

namespace List
{
    /// <summary>
    /// Двунаправленный связный список
    /// </summary>
    public class CustomLinkedList<T> : ICustomCollection<T>
        where T : IComparable<T>
    {
        private LinkedNode<T> head;

        ///<inheritdoc/>
        public void Add(T item)
        {
            if (IsEmpty())
            {
                head = new LinkedNode<T>() { Data = item };
            }
            else
            {
                var node = head;
                while(node.NextNode!=null)
                {
                    node = node.NextNode;
                }
                var newNode = new LinkedNode<T>() { Data = item, PrevNode = node };
                node.NextNode = newNode;
            }
        }
        ///<inheritdoc/>
        public void AddRange(T[] items)
        {
            if (IsEmpty())
            {
                head = new LinkedNode<T>() { Data = items[0] };
                var node = head;
                for (int i = 1; i < items.Length; i++)
                {
                    node.NextNode = new LinkedNode<T> { Data = items[i], PrevNode = node };
                    node = node.NextNode;
                }
            }
            else
            {
                var node = head;
                while (node.NextNode != null) node = node.NextNode;
                for (int i = 0; i < items.Length; i++)
                {
                    node.NextNode = new LinkedNode<T> { Data = items[i], PrevNode = node };
                    node = node.NextNode;
                }
            }
        }
        ///<inheritdoc/>
        public void Clear()
        {
            head = null;
        }
        ///<inheritdoc/>
        public bool Contains(T data)
        {
            LinkedNode<T> curNode = head;
            int k = 0;
            while (curNode != null)
                if (curNode.Data.CompareTo(data) == 0)
                    return true;
                else
                {
                    k++;
                    curNode = curNode.NextNode;
                }
            return false;
        }
        ///<inheritdoc/>
        public int IndexOf(T item)
        {
            LinkedNode<T> curNode = head;
            int k = 0;
            while (curNode != null)
                if (curNode.Data.CompareTo(item) == 0)
                    return k;
                else
                {
                    k++;
                    curNode = curNode.NextNode;
                }
            return -1;
        }
        ///<inheritdoc/>
        public void Insert(int index, T item)
        {
            LinkedNode<T> Ins = new LinkedNode<T>();
            Ins.Data = item;
            if (index < 0 || index > (Size() - 1))
                throw new IndexOutOfRangeException();
            if (index == 0)
            {
                Ins.NextNode = head;
                head.PrevNode = Ins;          
                head = Ins;
            }
            else
            {
                var node = head;
                for (var i = 0; i < index - 1; i++)
                {
                    node = node.NextNode;
                }
                node.NextNode.PrevNode = Ins;
                Ins.PrevNode = node;
                Ins.NextNode = node.NextNode;
                node.NextNode = Ins;
            }
        }
        ///<inheritdoc/>
        public bool IsEmpty()
        {
            return head == null;
        }
        ///<inheritdoc/>
        public void Remove(T item)
        {
            if (IsEmpty())
            {
                return;
            }

            if (head.Data.CompareTo(item) == 0)
            {
                head = head.NextNode;
                head.PrevNode = null;
                return;
            }
            LinkedNode<T> node = head;

            while (node.NextNode != null)
            {
                if (node.NextNode.Data.CompareTo(item) == 0 && node.NextNode.NextNode == null)
                {
                    node.NextNode = null;
                    break;
                }                 
                else if (node.NextNode.Data.CompareTo(item) == 0 && node.NextNode.NextNode != null)
                {
                    node.NextNode = node.NextNode.NextNode;
                    node.NextNode.PrevNode = node;
                    break;
                }
                else
                {
                    node = node.NextNode;
                }
            }
            return;
        }
        ///<inheritdoc/>
        public void RemoveAll(T item)
        {
            if (IsEmpty())
            {
                return;
            }

            while (head.Data.CompareTo(item) == 0)
            {
                head = head.NextNode;
                head.PrevNode = null;
            }
            LinkedNode<T> node = head;

            while (node.NextNode != null)
            {
                if (node.NextNode.Data.CompareTo(item) == 0 && node.NextNode.NextNode == null)
                {
                    node.NextNode = null;
                }
                else if (node.NextNode.Data.CompareTo(item) == 0 && node.NextNode.NextNode != null)
                {
                    node.NextNode = node.NextNode.NextNode;
                    node.NextNode.PrevNode = node;
                }
                else
                {
                    node = node.NextNode;
                }
            }
            return;
        }
        ///<inheritdoc/>
        public void RemoveAt(int index)
        {
            //проверка на выход за границы коллекции
            if (index < 0 || index > (Size() - 1))
                throw new IndexOutOfRangeException();


            //Удаление первого элемента
            if (index == 0)
            {
                head = head.NextNode;
                head.PrevNode = null;
            }
            else
            {
                var node = head;
                for (var i = 0; i < index; i++) {
                    node = node.NextNode;
                }
                node.NextNode = node.NextNode.NextNode;
                node.NextNode.PrevNode = node;
            }
        }
        ///<inheritdoc/>
        public void Reverse()
        {
            var node = head;
            while (node.NextNode!=null)
            {
                node = node.NextNode;
            }
            LinkedNode<T> pnode = new LinkedNode<T> { PrevNode = node.PrevNode, Data = node.Data, NextNode = null };
            while(pnode.PrevNode!=null)
            {
                if(node.NextNode == null)
                {
                    node.NextNode = pnode.PrevNode;
                    node.PrevNode = null;
                }
                else
                {
                    node.NextNode = pnode;
                    node.PrevNode = pnode.PrevNode;
                }
                node = node.NextNode;
                pnode = pnode.PrevNode;
                
            }
        }
        ///<inheritdoc/>
        public int Size()
        {
            if (head == null) return 0;
            int count = 1;
            LinkedNode<T> curNode = head;
            while (curNode.NextNode != null)
            {
                count++;
                curNode = curNode.NextNode;
            }
            return count;
        }
        public void Display()
        {
            if (!IsEmpty())
            {
                LinkedNode<T> node = head;
                while (node != null)
                {
                    Console.Write(node.Data + " ");
                    node = node.NextNode;
                }

                Console.WriteLine();
            }
            else Console.WriteLine("list empty");
        }
    }
}
