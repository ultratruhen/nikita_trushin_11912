﻿using System;
using System.Collections.Generic;
using System.Text;

namespace List
{
    class CustomArrList<T> : ICustomCollection<T>
        where T : IComparable<T>
    {
        ArrList<T> list;

        public void Enlarge()
        {
            ArrList<T> newlist = default;
            newlist.Data = new T[list.Data.Length * 2];
            newlist.LastIndex = list.LastIndex;
            for (int i = 0; i<list.Data.Length; i++)
            {
                newlist.Data[i] = list.Data[i];
            }
            list = newlist;
        }

        private void CreateNewList()
        {
            list.Data = new T[1];
            list.LastIndex = 0;
        }
        public void Add(T item)
        {
            if(list.Data == null)
            {
                CreateNewList();
                list.Data[0] = item;
            }
            else if (list.LastIndex == list.Data.Length)
            {
                Enlarge();
                list.LastIndex++;
                list.Data[list.LastIndex] = item;
            }
            else
            {
                list.LastIndex++;
                list.Data[list.LastIndex] = item;
            }
        }

        public void AddRange(T[] items)
        {
            throw new NotImplementedException();
        }

        public void Clear()
        {
            throw new NotImplementedException();
        }

        public bool Contains(T data)
        {
            throw new NotImplementedException();
        }

        public int IndexOf(T item)
        {
            throw new NotImplementedException();
        }

        public void Insert(int index, T item)
        {
            throw new NotImplementedException();
        }

        public bool IsEmpty()
        {
            throw new NotImplementedException();
        }

        public void Remove(T item)
        {
            throw new NotImplementedException();
        }

        public void RemoveAll(T item)
        {
            throw new NotImplementedException();
        }

        public void RemoveAt(int index)
        {
            throw new NotImplementedException();
        }

        public void Reverse()
        {
            throw new NotImplementedException();
        }

        public int Size()
        {
            throw new NotImplementedException();
        }
    }
}
