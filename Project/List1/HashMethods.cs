﻿using System;
using System.Collections.Generic;
using System.Text;

namespace List
{
    class HashMethods
    {
        HashTable head;
        public void Add(int key, int data)
        {
            if (IsEmpty())
            {
                head = new HashTable { Data = data, Key = key };
            }
            while (head.NextNode != null)
            {
                head = head.NextNode;
            }
            head.NextNode = new HashTable { Data = data, Key = key };
        }
        public void DeleteNode(int key)
        {
            if (IsEmpty())
            {
                throw new NullReferenceException();
            }
            while (head.NextNode.Key != key)
            {
                head = head.NextNode;
                if (head.NextNode == null)
                    return;
            }
            head.NextNode = head.NextNode.NextNode;
        }
        public HashTable FindByKey(int key)
        {
            if (IsEmpty())
            {
                throw new NullReferenceException();
            }
            while (head.Key != key)
            {
                if (head.NextNode == null)
                    throw new NullReferenceException();
            }
            return head;
        }

        public bool IsEmpty()
        {
            return head == null;
        }
    }
}
