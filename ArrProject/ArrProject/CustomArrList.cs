﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ArrProject
{
    class CustomArrList<T> : ICustomCollection<T>
        where T : IComparable<T>
    {
        ArrList<T> list;

        public void Enlarge()
        {
            ArrList<T> newlist = default;
            newlist.Data = new T[list.Data.Length * 2];
            newlist.LastIndex = list.LastIndex;
            for (int i = 0; i<list.Data.Length; i++)
            {
                newlist.Data[i] = list.Data[i];
            }
            list = newlist;
        }

        private void CreateNewList()
        {
            list.Data = new T[1];
            list.LastIndex = 0;
        }
        public void Add(T item)
        {
            if(list.Data == null)
            {
                CreateNewList();
                list.Data[0] = item;
            }
            else if (list.LastIndex == list.Data.Length)
            {
                Enlarge();
                list.LastIndex++;
                list.Data[list.LastIndex] = item;
            }
            else
            {
                list.LastIndex++;
                list.Data[list.LastIndex] = item;
            }
        }

        public void AddRange(T[] items)
        {
            if (list.Data == null)
            {
                CreateNewList();
                for (int i = 0; i < items.Length; i++)
                {
                    if (list.LastIndex == list.Data.Length)
                    {
                        Enlarge();
                        list.LastIndex++;
                        list.Data[list.LastIndex] = items[i];
                    }
                }
            }
            else if (list.LastIndex == list.Data.Length)
            {
                Enlarge();
                for (int i = 0; i < items.Length; i++)
                {
                    if (list.LastIndex == list.Data.Length)
                    {
                        Enlarge();
                        list.LastIndex++;
                        list.Data[list.LastIndex] = items[i];
                    }
                }
            }
            else
            {
                for (int i = 0; i < items.Length; i++)
                {
                    if (list.LastIndex == list.Data.Length)
                    {
                        Enlarge();
                        list.LastIndex++;
                        list.Data[list.LastIndex] = items[i];
                    }
                }
            }
        }

        public void Clear()
        {
            list = null;
        }

        public bool Contains(T data)
        {
            for(int i=0;i<list.LastIndex;i++)
            {
                if (list.Data[i].CompareTo(data)==0)
                {
                    return true;              
                }
            }
            return false;
        }

        public int IndexOf(T item)
        {
            for (int i = 0; i < list.LastIndex; i++)
            {
                if (list.Data[i].CompareTo(item) == 0)
                {
                    return i;
                }
            }
            return -1;
        }

        public void Insert(int index, T item)
        {
            if (list.LastIndex == list.Data.Length)
            {
                Enlarge();
            }
            for (int i = list.LastIndex; i > index; i--)
            {
                list.Data[i + 1] = list.Data[i];
            }
            list.Data[index] = item;
            list.LastIndex++;

        }

        public bool IsEmpty()
        {
            return list == null;
        }

        public void Remove(T item)
        {
            int index = -1;
            for(int i=0;i<list.LastIndex;i++)
            {
                if (list.Data[i].CompareTo(item) == 0)
                {
                    index = i;
                    break;
                }
            }
            if (index == list.LastIndex)
            {
                list.Data[index] = default;
                list.LastIndex--;
            }
            else if (index != -1)
            {
                for (int i = index + 1; i < list.LastIndex; i++)
                {
                    list.Data[i - 1] = list.Data[i];               
                }
                list.Data[index] = default;
                list.LastIndex--;
            }
        }

        public void RemoveAll(T item)
        {
            for(int i = 0; i<list.LastIndex;i++)
            {
                Remove(item);
            }
        }

        public void RemoveAt(int index)
        {
            if(index == list.LastIndex)
            {
                list.Data[index] = default;
                list.LastIndex--;
            }
            else
            {
                for (int i = index + 1; i < list.LastIndex; i++)
                {
                    list.Data[i - 1] = list.Data[i];
                }
                list.Data[index] = default;
                list.LastIndex--;
            }
        }

        public void Reverse()
        {
            if(list.LastIndex == list.Data.Length)
                Array.Reverse(list.Data);
            else
            {
                Array.Reverse(list.Data);
                int index = 0;
                for(int i = list.Data.Length-list.LastIndex; i < list.Data.Length;i++)
                {
                    list.Data[index] = list.Data[i];
                    index++;
                }
                for (int i = list.LastIndex; i < list.Data.Length; i++)
                {
                    list.Data[i] = default;
                }

            }
        }

        public int Size()
        {
            return list.LastIndex + 1;
        }
    }
}
