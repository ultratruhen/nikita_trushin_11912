﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ArrProject
{
    public class ArrList<T>
    {
        public T[] Data { get; set; }
        
        public int LastIndex { get; set; }
    }
}
