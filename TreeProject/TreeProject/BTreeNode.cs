﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreeProject
{
    class BTreeNode<T>
    {
        public BTreeNode(int key, T data, BTreeNode<T> parent = null)
        {
            Data = data;
            Key = key;
            Parent = parent;
        }
        public BTreeNode(int key, T data, BTreeNode<T> l, BTreeNode<T> r, BTreeNode<T> p)
        {
            Key = key;
            Data = data;
            Left = l;
            Right = r;
            Parent = p;
        }
        public BTreeNode()
        {

        }
        /// <summary>
        /// Ключ
        /// </summary>
        public int Key { get; set; }

        /// <summary>
        /// данные
        /// </summary>
        public T Data { get; set; }

        /// <summary>
        /// левая ветка дерева
        /// </summary>
        public BTreeNode<T> Left { get; set; }

        /// <summary>
        /// правая ветка дерева
        /// </summary>
        public BTreeNode<T> Right { get; set; }

        /// <summary>
        /// Родительский элемент
        /// </summary>
        public BTreeNode<T> Parent { get; set; }
    }
}
