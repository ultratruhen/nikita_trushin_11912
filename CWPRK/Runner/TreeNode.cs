﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Runner
{
    class BTreeNode
    {
        public int Key { get; set; }
        public int Data { get; set; }
        public BTreeNode LeftChild { get; set; }
        public BTreeNode RightChild { get; set; }
        public BTreeNode Parent { get; set; }
        public BTreeNode(int key, int data, BTreeNode l, BTreeNode r, BTreeNode p)
        {
            Key = key;
            Data = data;
            LeftChild = l;
            RightChild = r;
            Parent = p;
        }
        public BTreeNode()
        {

        }
    }
}
