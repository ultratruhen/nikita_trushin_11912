﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace Runner
{
    class Program
    {
        public delegate void Del();
        static void Main(string[] args)
        {
            LinqRunner test = new LinqRunner();
            test.Run();
            Console.WriteLine("LINQ - GET");
            Console.ReadKey();

            EventRunner test1 = new EventRunner();
            test1.Run();
            Console.WriteLine("EVENTS - GET");
            Console.ReadKey();

            ReflectionRunner test2 = new ReflectionRunner();
            test2.Run();
            Console.WriteLine("REFLECTION - GET");
            Console.ReadKey();
            

            TreeRunner test3 = new TreeRunner();
            test3.Run();
            Console.WriteLine("TREE - GET");
        }


    }
}
