﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Runner

{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
    


    public class Price
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public int Sum { get; set; }
        public bool IsActual { get; set; }
    }



    public class Payment
    {
        public string Name { get; set; }
        public int Count { get; set; }
    }



    public class Sale
    {
        public double Percent { get; set; }
        public List<string> Products { get; set; }
    }


    public class SaleType
    {
        public List<string> Set { get; set; }
        public double Before { get; set; }
        public double Sale { get; set; }
    }


    public class SaleCheck
    {
        public int Aqua { get; set; }
        public int Filter { get; set; }
    }
}
