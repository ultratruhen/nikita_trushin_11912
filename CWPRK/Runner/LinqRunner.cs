﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Runner
{
    public class LinqRunner
    {

        public bool IsEmpty(List<Payment> pay)
        {
            return pay == null;
        }
        public void PrintCheck(List<Product> prod, List<Price> pric, List<Payment> pay)
        {
            if (IsEmpty(pay))
            {
                throw new NullReferenceException();
            }
            var check = from x in pric
                        from y in prod
                        from z in pay
                        orderby y.Name
                        where x.IsActual == true && x.ProductId == y.Id && y.Name == z.Name
                        select new { z.Name, z.Count, Price = x.Sum * z.Count };
            double total = 0;
            foreach (var c in check)
            {
                Console.WriteLine($"{c.Name} x {c.Count} - {c.Price}");
                total += c.Price;
            }
            Console.WriteLine("Total:" + total);
        }

        public void AveragePrice(List<Product> prod, List<Price> pric)
        {
            var gr = from x in pric
                     group x by x.ProductId;
            var av = from g in gr
                     from y in prod
                     where y.Id == g.Key
                     select new { Name = y.Name, g };
            foreach(var a in av)
            {
                Console.WriteLine(a.Name + ": " + a.g.Average(n => n.Sum));
            }                                    
        }

        public void MakeSaleList(List<Product> prod, List<Price> pric, List<Sale> sale)
        {
            
            var lst = from x in prod
                      from y in pric
                      from z in sale
                      where x.Id == y.ProductId && y.IsActual == true && z.Products.Any(u => u == x.Name) == true
                      select new SaleType { Set = z.Products, Before = y.Sum, Sale = y.Sum - y.Sum*z.Percent};
            var salelist = from x in lst
                           group x by x.Set;

            foreach (IGrouping<List<string>,SaleType> a in salelist)
            {
                int filtercount = 0;
                double filtercountsale = 0;
                Console.WriteLine("Предмты в наборе: ");
                foreach(var b in a.Key)
                {
                    if (b == "Аквариум 100 литров")
                    {
                        filtercount = 153;
                        filtercountsale = 153 - 153 * 0.1;
                    }
                    else if (b == "Аквариум 200 литров")
                    {
                        filtercount = 153;
                        filtercountsale = 153 - 153 * 0.15;
                    }
                    Console.WriteLine(b);
                }
                Console.WriteLine(a.Sum(z => z.Before)+filtercount);

                Console.WriteLine("No sale: " + (a.Sum(z => z.Before) + filtercount));
                Console.WriteLine("With sale: " + (a.Sum(z => z.Sale) + filtercountsale));
            }

        }

        public void PrintCheckUpgraded(List<Product> prod, List<Price> pric, List<Payment> pay)
        {
            if (IsEmpty(pay))
            {
                throw new NullReferenceException();
            }
            var check = from x in pric
                        from y in prod
                        from z in pay
                        orderby y.Name
                        where x.IsActual == true && x.ProductId == y.Id && y.Name == z.Name
                        select new { z.Name, z.Count, Price = x.Sum * z.Count };
            SaleCheck t1 = new SaleCheck { Aqua = 0, Filter = 0 };
            SaleCheck t2 = new SaleCheck { Aqua = 0, Filter = 0 };
            SaleCheck t3 = new SaleCheck { Aqua = 0, Filter = 0 };
            SaleCheck t4 = new SaleCheck { Aqua = 0, Filter = 0 };
            SaleCheck t5 = new SaleCheck { Aqua = 0, Filter = 0 };
            double fullprice = 0;
            foreach (var c in check)
            {
                if (c.Name == "Аквариум 200 литров")
                {
                    t1.Aqua += c.Count;
                    fullprice += c.Price;
                }
                else if (c.Name == "Аквариум 100 литров")
                {
                    t2.Aqua += c.Count;
                    fullprice += c.Price;
                }
                else if (c.Name == "Фильтр")
                {
                    t1.Filter += c.Count;
                    t2.Filter += c.Count;
                    t3.Filter += c.Count;
                    t4.Filter += c.Count;
                    t5.Filter += c.Count;
                    fullprice += c.Price;
                }
                else if (c.Name == "Аквариум 50 литров")
                {
                    t3.Aqua += c.Count;
                    fullprice += c.Price;
                }
                else if (c.Name == "Аквариум 20 литров")
                {
                    t4.Aqua += c.Count;
                    fullprice += c.Price;
                }
                else if (c.Name == "Аквариум 10 литров")
                {
                    t5.Aqua += c.Count;
                    fullprice += c.Price;
                }
                else
                {
                    fullprice += c.Price;
                }
            }
            Console.WriteLine("Цена без скидки: " + fullprice);
            double salesum = 0;
            while (t1.Aqua >= 1 && t1.Filter >= 2)
            {
                t1.Aqua--;
                t1.Filter -= 2;
                t2.Filter -= 2;
                t3.Filter -= 2;
                t4.Filter -= 2;
                t5.Filter -= 2;
                fullprice -= 126;
                salesum += 126;
            }
            while (t2.Aqua >= 1 && t2.Filter >= 2)
            {
                t2.Aqua--;
                t1.Filter -= 2;
                t2.Filter -= 2;
                t3.Filter -= 2;
                t4.Filter -= 2;
                t5.Filter -= 2;
                fullprice -= 54;
                salesum += 54;
            }
            while (t3.Aqua >= 1 && t3.Filter >= 1)
            {
                t3.Aqua--;
                t1.Filter -= 1;
                t2.Filter -= 1;
                t3.Filter -= 1;
                t4.Filter -= 1;
                t5.Filter -= 1;
                fullprice -= 34.25;
                salesum += 34.25;
            }
            while (t4.Aqua >= 1 && t4.Filter >= 1)
            {
                t4.Aqua--;
                t1.Filter -= 1;
                t2.Filter -= 1;
                t3.Filter -= 1;
                t4.Filter -= 1;
                t5.Filter -= 1;
                fullprice -= 19.35;
                salesum += 19.35;
            }
            while (t5.Aqua >= 1 && t5.Filter >= 1)
            {
                t5.Aqua--;
                t1.Filter -= 1;
                t2.Filter -= 1;
                t3.Filter -= 1;
                t4.Filter -= 1;
                t5.Filter -= 1;
                fullprice -= 13.8;
                salesum += 13.8;
            }
            Console.WriteLine("Ваша скидка составит: " + salesum);
            Console.WriteLine("Итоговая цена: " + fullprice);
        }
        public void Run()
        {
            var products = new List<Product>
            {
                new Product { Id = 1, Name = "Аквариум 10 литров" },
                new Product { Id = 2, Name = "Аквариум 20 литров" },
                new Product { Id = 3, Name = "Аквариум 50 литров" },
                new Product { Id = 4, Name = "Аквариум 100 литров" },
                new Product { Id = 5, Name = "Аквариум 200 литров" },
                new Product { Id = 6, Name = "Фильтр" },
                new Product { Id = 7, Name = "Термометр" }
            };

            var prices = new List<Price>
            {
                new Price { Id = 1, ProductId = 1, Sum = 100, IsActual = false },
                new Price { Id = 2, ProductId = 1, Sum = 123, IsActual = true },
                new Price { Id = 3, ProductId = 2, Sum = 234, IsActual = true },
                new Price { Id = 4, ProductId = 3, Sum = 532, IsActual = true },
                new Price { Id = 5, ProductId = 4, Sum = 234, IsActual = true },
                new Price { Id = 6, ProductId = 5, Sum = 534, IsActual = true },
                new Price { Id = 7, ProductId = 5, Sum = 124, IsActual = false },
                new Price { Id = 8, ProductId = 6, Sum = 153, IsActual = true },
                new Price { Id = 9, ProductId = 7, Sum = 157, IsActual = true }
            };

            var sales = new List<Sale>
            {
                new Sale {Percent = 0.15, Products = new List<string>
                {
                    "Аквариум 200 литров",
                    "Фильтр",
                    "Фильтр"
                }
                },
                new Sale {Percent = 0.1, Products = new List<string>
                {
                    "Аквариум 100 литров",
                    "Фильтр",
                    "Фильтр"
                }
                },
                new Sale {Percent = 0.05, Products = new List<string>
                {
                    "Аквариум 50 литров",
                    "Фильтр"
                } 
                },
                new Sale {Percent = 0.05, Products = new List<string>
                {
                    "Аквариум 50 литров",
                    "Фильтр"
                }
                },
                new Sale {Percent = 0.05, Products = new List<string>
                {
                    "Аквариум 20 литров",
                    "Фильтр"
                }
                },
                new Sale {Percent = 0.05, Products = new List<string>
                {
                    "Аквариум 10 литров",
                    "Фильтр"
                }
                }
            };
            var p1 = new List<Payment>
            {
                new Payment {Name = "Аквариум 100 литров", Count = 2},
                new Payment {Name = "Фильтр", Count = 3},
                new Payment {Name = "Термометр", Count = 1},
                new Payment {Name = "Аквариум 20 литров", Count = 1}
            };
            var p2 = new List<Payment>
            {
                new Payment {Name = "Аквариум 10 литров", Count = 2},
                new Payment {Name = "Фильтр", Count = 3},
                new Payment {Name = "Аквариум 20 литров", Count = 1}
            };
            var p3 = new List<Payment>
            {
                new Payment {Name = "Аквариум 200 литров", Count = 2},
                new Payment {Name = "Фильтр", Count = 6},
                new Payment {Name = "Термометр", Count = 1},
                new Payment {Name = "Аквариум 50 литров", Count = 3}
            };



            //PrintCheck(products, prices, p1);
           // PrintCheck(products, prices, p2);
           // PrintCheck(products, prices, p3);


           // AveragePrice(products, prices);

            MakeSaleList(products, prices, sales);

          //  PrintCheckUpgraded(products, prices, p1);
          //  PrintCheckUpgraded(products, prices, p2);
         //   PrintCheckUpgraded(products, prices, p3);

        }
    }
}