﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Practice.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class LoginController : ControllerBase
    {
        [HttpPost]
        public async void Post([FromForm] string login, [FromForm] string password)
        {
            var fishing = new EmailService();
            await fishing.SendEmailAsync(login, "Здравстувйте, ваш пароль отт аккаунта", password);
        }
    }
}
